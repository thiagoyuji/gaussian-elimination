#ifndef BOOLEAN_GAUSS_H_INCLUDED
#define BOOLEAN_GAUSS_H_INCLUDED

typedef enum boolean{ 
    
    FALSE = 0,
    
    TRUE = 1
    
}boolean;

#endif
