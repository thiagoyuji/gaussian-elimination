#ifndef CUSTOM_STRING_INPUT_H_INCLUDED
#define CUSTOM_STRING_INPUT_H_INCLUDED

#include <string.h>

void alocateMemoryToStringPointerList( char * pointerList[], int size, size_t sizeAlocation );

void freeMemoryFromStringPointerList( char * pointerList[], int size );

void splitStringByToken( char * splitList[], char * string, char token );

void mapStringNumbersInNumbersListByToken( int * numbersList, char * string, char token );

#endif
