#include <stdlib.h>

#include "string.h"

void alocateMemoryToStringPointerList( char * pointerList[], int size, size_t sizeAlocation ){
    
    int count = 0;
    
    for( count = 0; count < size; count++ ){
        
        pointerList[ count ] = ( char * ) malloc( sizeAlocation );
        
    }
    
}

void freeMemoryFromStringPointerList( char * pointerList[], int size ){
    
    int count = 0;
    
    for( count = 0; count < size; count++ ){
        
        free( pointerList[ count ] ); 
        
    }
    
}

void splitStringByToken( char * splitList[], char * string, char token ){
    
    long unsigned int sizeStringCount = 0;
    
    int pieceCount = 0;
    
    int charCount = 0;
    
    for( sizeStringCount = 0; sizeStringCount < strlen( string ); sizeStringCount++ ){
        
        if( string[ sizeStringCount ] == token ){
            
            splitList[ pieceCount++ ][ charCount ] = '\0';
            
            sizeStringCount++;
            
            charCount = 0;
            
        }
        
        splitList[ pieceCount ][ charCount++ ] = string[ sizeStringCount ];
        
    }
    
    splitList[ pieceCount++ ][ charCount ] = '\0';
    
}

void mapStringNumbersInNumbersListByToken( int * numbersList, char * string, char token ){
    
    char auxiliar[ strlen( string ) ];
    
    long unsigned int sizeStringCount = 0;
    
    int pieceCount = 0;
    
    int numberCount = 0;
    
    for( sizeStringCount = 0; sizeStringCount < strlen( string ); sizeStringCount++ ){
        
        if( string[ sizeStringCount ] == token ){
            
            auxiliar[ pieceCount ] = '\0';
            
            numbersList[ numberCount++ ] = atoi( auxiliar );
            
            memset( auxiliar, 0, strlen( string ) );
            
            sizeStringCount++;
            
            pieceCount = 0;
            
        }
        
        auxiliar[ pieceCount++ ] = string[ sizeStringCount ];
        
    }
    
    numbersList[ numberCount++ ] = atoi( auxiliar );
    
}
