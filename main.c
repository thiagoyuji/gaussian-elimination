#include <stdio.h>

#include "console/console.h"
#include "src/gaussian_elimination/gaussian_elimination.h"

int main(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 4,
        .numberOfIcognitos = 4,
        .linearExpressions = "1 2 3 4 5/5 4 3 2 1/6 11 2 10 6/7 3 5 4 3"
    };
    
    output( applyGaussianEliminationOn( request ) );    
    
    return 0;
    
}
