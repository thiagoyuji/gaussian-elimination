#ifndef PROTOCOL_H_INCLUDED
#define PROTOCOL_H_INCLUDED

#define SUCCESS_MESSAGE "Success : Gaussian Elimination Completed"

enum SuccessProtocolCode {
  
    SUCCESS = 1000
    
};

#define INVALID_REQUEST_MESSAGE "Error : Invalid Gaussian Elimination Request"
#define CANNOT_SCALE_MATRIX_MESSAGE "Error : Matrix Requested cannot be scaled"

enum ErrorProtocolCode {
  
    INVALID_REQUEST = 2000,
    CANNOT_SCALE_MATRIX = 2001
    
};

#endif
