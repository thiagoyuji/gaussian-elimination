#include <stdlib.h>
#include <assert.h>

#include "../../include/boolean/boolean.h"
#include "../../src/predicate/predicate.h"

void sceneValidRequestNumberOfLinearExpressions();

void sceneInvalidRequestNumberOfLinearExpressions();

void sceneValidRequestNumberOfIcognitos();

void sceneInvalidRequestNumberOfIcognitos();

void sceneInvalidRequestNumberOfIcognitosDifferentNumberOfLinearExpressions();

void sceneValidRequestLinearExpressions();

void sceneInvalidRequestLinearExpressionsEqualEmpty();

void sceneInvalidRequestLinearExpressionsEqualNull();

void sceneInvalidRequestLinearExpressionsNotMatchRegex();

int main(){
    
    sceneValidRequestNumberOfLinearExpressions();
    
    sceneInvalidRequestNumberOfLinearExpressions();
    
    sceneValidRequestNumberOfIcognitos();
    
    sceneInvalidRequestNumberOfIcognitos();
    
    sceneInvalidRequestNumberOfIcognitosDifferentNumberOfLinearExpressions();
    
    sceneValidRequestLinearExpressions();
    
    sceneInvalidRequestLinearExpressionsEqualEmpty();
    
    sceneInvalidRequestLinearExpressionsEqualNull();
    
    sceneInvalidRequestLinearExpressionsNotMatchRegex();
    
    return 0;
    
}

void sceneValidRequestNumberOfLinearExpressions(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = -1,
        .linearExpressions = ""
    };
    
    assert( predicateNumberOfLinearExpressionsValid( request ) == TRUE );
    
}

void sceneInvalidRequestNumberOfLinearExpressions(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 0,
        .numberOfIcognitos = -1,
        .linearExpressions = ""
    };
    
    assert( predicateNumberOfLinearExpressionsValid( request ) == FALSE );
    
}

void sceneValidRequestNumberOfIcognitos(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 3,
        .linearExpressions = ""
    };
    
    assert( predicateNumberOfIcognitosValid( request ) == TRUE );
    
}

void sceneInvalidRequestNumberOfIcognitos(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = -1,
        .numberOfIcognitos = 0,
        .linearExpressions = ""
    };
    
    assert( predicateNumberOfIcognitosValid( request ) == FALSE );
    
}

void sceneInvalidRequestNumberOfIcognitosDifferentNumberOfLinearExpressions(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 2,
        .linearExpressions = ""
    };
    
    assert( predicateNumberOfIcognitosValid( request ) == FALSE );
    
}

void sceneValidRequestLinearExpressions(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 3,
        .linearExpressions = "-1 +2 3 -6/-15345 +2353 3354 -6454/-1 +2 3 -6"
    };
    
    assert( predicateStringLinearExpressionsValid( request ) == TRUE );
    
}

void sceneInvalidRequestLinearExpressionsEqualEmpty(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 3,
        .linearExpressions = ""
    };
    
    assert( predicateStringLinearExpressionsValid( request ) == FALSE );
    
}

void sceneInvalidRequestLinearExpressionsEqualNull(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 3,
        .linearExpressions = NULL
    };
    
    assert( predicateStringLinearExpressionsValid( request ) == FALSE );
    
}

void sceneInvalidRequestLinearExpressionsNotMatchRegex(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 3,
        .linearExpressions = "/-1 +2 3 -6/-153453 +2353 33545345345 -6454/-1 +2 3 -6"
    };
    
    assert( predicateStringLinearExpressionsValid( request ) == FALSE );
    
}
