#include <assert.h>
#include <stdio.h>

#include "../../../src/input/gaussianEliminationRequest.h"
#include "../../../src/linear_expressions_matrix/conversor/conversor.h"

void sceneConversionSuccess();

int main(){
    
    sceneConversionSuccess();
    
    return 0;
    
}

void sceneConversionSuccess(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 3,
        .linearExpressions = "1 2 3 6/0 4 2 7/1 0 1 8"
    };
    
    struct LinearExpressionsMatrix matrix = convert( request );
    
    assert( matrix.linearExpressions[ 0 ][ 0 ] == 1 );
    assert( matrix.linearExpressions[ 0 ][ 1 ] == 2 );
    assert( matrix.linearExpressions[ 0 ][ 2 ] == 3 );
    assert( matrix.linearExpressions[ 0 ][ 3 ] == 6 );
    
    assert( matrix.linearExpressions[ 1 ][ 0 ] == 0 );
    assert( matrix.linearExpressions[ 1 ][ 1 ] == 4 );
    assert( matrix.linearExpressions[ 1 ][ 2 ] == 2 );
    assert( matrix.linearExpressions[ 1 ][ 3 ] == 7 );
    
    assert( matrix.linearExpressions[ 2 ][ 0 ] == 1 );
    assert( matrix.linearExpressions[ 2 ][ 1 ] == 0 );
    assert( matrix.linearExpressions[ 2 ][ 2 ] == 1 );
    assert( matrix.linearExpressions[ 2 ][ 3 ] == 8 );
    
    freeMatrix( matrix );
    
}
