#include <assert.h>

#include "../../src/linear_expressions_matrix/matrix.h"
#include "../../include/zero.h"

void caseCreateMatrix();

int main(){
    
    caseCreateMatrix();
    
    return 0;
    
}

void caseCreateMatrix(){
 
    struct LinearExpressionsMatrix matrix = createMatrix( 3, 3 );
    
    int line = 0;
    
    int column = 0;
    
    for( line = 0; line < 3; line++){
        
        for( column = 0; column < 3; column++ ){
            
            assert( matrix.linearExpressions[ line ][ column ] == ZERO );
            
        }
        
    }
    
    freeMatrix( matrix );
    
}
