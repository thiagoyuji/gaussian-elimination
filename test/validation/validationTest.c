#include <assert.h>

#include "../../include/boolean/boolean.h"
#include "../../src/validation/validation.h"
#include "../../src/input/gaussianEliminationRequest.h"

void sceneValidRequest();

void sceneInvalidRequest();

int main(){
    
    sceneValidRequest();
    
    sceneInvalidRequest();
    
    return 0;
    
}

void sceneValidRequest(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 3,
        .numberOfIcognitos = 3,
        .linearExpressions = "-1 +2 3 -6/-15345 +2353 3354 -6454/-1 +2 3 -6"
    };
    
    assert( validateRequest( request ) == TRUE );
    
}

void sceneInvalidRequest(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 0,
        .numberOfIcognitos = 0,
        .linearExpressions = ""
    };
    
    assert( validateRequest( request ) == FALSE );
    
}
