#include <assert.h>
#include <string.h>

#include "../../src/linear_expressions_matrix/matrix.h"
#include "../../src/algorithm/algorithm.h"
#include "../../src/output/gaussianEliminationResponse.h"
#include "../../protocol/protocol.h"

void sceneScaleErrorColumnPivotValuesEqualZero();

void sceneScaleSuccess3x3();

void sceneScaleSuccess4x4();

int main(){
    
    sceneScaleErrorColumnPivotValuesEqualZero();
    
    sceneScaleSuccess3x3();
    
    sceneScaleSuccess4x4();
    
    return 0;
    
}

void sceneScaleErrorColumnPivotValuesEqualZero(){
    
    struct LinearExpressionsMatrix matrix = createMatrix( 3, 3 );
    
    struct GaussianEliminationResponse response =  gaussianEliminationAlgorithm( matrix );
    
    assert( response.code == CANNOT_SCALE_MATRIX );
    
    assert( strcmp( response.message, CANNOT_SCALE_MATRIX_MESSAGE) == 0 );    
    
    freeMatrix( matrix );
    
}

void sceneScaleSuccess3x3(){
    
    struct LinearExpressionsMatrix matrix = createMatrix( 3, 4 );
    
    matrix.linearExpressions[ 0 ][ 0 ] = 1;
    matrix.linearExpressions[ 0 ][ 1 ] = 2;
    matrix.linearExpressions[ 0 ][ 2 ] = 3;
    matrix.linearExpressions[ 0 ][ 3 ] = 6;
    
    matrix.linearExpressions[ 1 ][ 0 ] = 0;
    matrix.linearExpressions[ 1 ][ 1 ] = 4;
    matrix.linearExpressions[ 1 ][ 2 ] = 2;
    matrix.linearExpressions[ 1 ][ 3 ] = 7;
    
    matrix.linearExpressions[ 2 ][ 0 ] = 1;
    matrix.linearExpressions[ 2 ][ 1 ] = 0;
    matrix.linearExpressions[ 2 ][ 2 ] = 1;
    matrix.linearExpressions[ 2 ][ 3 ] = 8;
    
    struct GaussianEliminationResponse response =  gaussianEliminationAlgorithm( matrix );
    
    assert( response.code == SUCCESS );
    
    assert( strcmp( response.message, SUCCESS_MESSAGE) == 0 );  
    
    assert( strcmp( response.calculationResults, "[x3 = -5.500][x2 = 4.500][x1 = 13.500]") == 0 );
    
    freeMatrix( matrix );
    
}

void sceneScaleSuccess4x4(){
    
    struct LinearExpressionsMatrix matrix = createMatrix( 4, 5 );
    
    matrix.linearExpressions[ 0 ][ 0 ] = 1;
    matrix.linearExpressions[ 0 ][ 1 ] = 2;
    matrix.linearExpressions[ 0 ][ 2 ] = 3;
    matrix.linearExpressions[ 0 ][ 3 ] = 4;
    matrix.linearExpressions[ 0 ][ 4 ] = 5;
    
    matrix.linearExpressions[ 1 ][ 0 ] = 5;
    matrix.linearExpressions[ 1 ][ 1 ] = 4;
    matrix.linearExpressions[ 1 ][ 2 ] = 3;
    matrix.linearExpressions[ 1 ][ 3 ] = 2;
    matrix.linearExpressions[ 1 ][ 4 ] = 1;
    
    matrix.linearExpressions[ 2 ][ 0 ] = 6;
    matrix.linearExpressions[ 2 ][ 1 ] = 11;
    matrix.linearExpressions[ 2 ][ 2 ] = 2;
    matrix.linearExpressions[ 2 ][ 3 ] = 10;
    matrix.linearExpressions[ 2 ][ 4 ] = 6;
    
    matrix.linearExpressions[ 3 ][ 0 ] = 7;
    matrix.linearExpressions[ 3 ][ 1 ] = 3;
    matrix.linearExpressions[ 3 ][ 2 ] = 5;
    matrix.linearExpressions[ 3 ][ 3 ] = 4;
    matrix.linearExpressions[ 3 ][ 4 ] = 3;
    
    struct GaussianEliminationResponse response =  gaussianEliminationAlgorithm( matrix );
    
    assert( response.code == SUCCESS );
    
    assert( strcmp( response.message, SUCCESS_MESSAGE) == 0 );  
    
    assert( strcmp( response.calculationResults, "[x4 = 0.800][x3 = 0.800][x2 = -0.000][x1 = -0.600]") == 0 );
    
    freeMatrix( matrix );
    
}
