#include <assert.h>
#include <stdlib.h>

#include "../../../include/zero.h"
#include "../../../include/string/string.h"

void caseSplitStringByToken();

void caseMapStringNumbersInNumbersListByToken();

int main(){
    
    caseSplitStringByToken();
    
    caseMapStringNumbersInNumbersListByToken();
    
    return 0;
    
}

void caseSplitStringByToken(){
    
    char * linearExpressions[ 3 ];
    
    char * stringMatrix = "1 2 3 6/0 4 2 7/1 0 1 8";
    
    alocateMemoryToStringPointerList( linearExpressions, 3, strlen( stringMatrix ) );
    
    splitStringByToken( linearExpressions, stringMatrix, '/' );
    
    assert( strcmp( linearExpressions[0], "1 2 3 6" ) == ZERO );
    assert( strcmp( linearExpressions[1], "0 4 2 7" ) == ZERO );
    assert( strcmp( linearExpressions[2], "1 0 1 8" ) == ZERO );
    
    freeMemoryFromStringPointerList( linearExpressions, 3 );
    
}

void caseMapStringNumbersInNumbersListByToken(){
    
    char * linearExpressions[ 3 ];
    
    char * stringMatrix = "1 2 3 6/0 4 2 7/1 0 1 8";
    
    alocateMemoryToStringPointerList( linearExpressions, 3, strlen( stringMatrix ) );
    
    splitStringByToken( linearExpressions, stringMatrix, '/' );
    
    int numbers[ 4 ];
    
    mapStringNumbersInNumbersListByToken( numbers, linearExpressions[ 0 ], ' ' );
    
    assert( numbers[ 0 ] == 1 );
    assert( numbers[ 1 ] == 2 );
    assert( numbers[ 2 ] == 3 );
    assert( numbers[ 3 ] == 6 );
    
    mapStringNumbersInNumbersListByToken( numbers, linearExpressions[ 1 ], ' ' );
    
    assert( numbers[ 0 ] == 0 );
    assert( numbers[ 1 ] == 4 );
    assert( numbers[ 2 ] == 2 );
    assert( numbers[ 3 ] == 7 );
    
    mapStringNumbersInNumbersListByToken( numbers, linearExpressions[ 2 ], ' ' );
    
    assert( numbers[ 0 ] == 1 );
    assert( numbers[ 1 ] == 0 );
    assert( numbers[ 2 ] == 1 );
    assert( numbers[ 3 ] == 8 );
    
    freeMemoryFromStringPointerList( linearExpressions, 3 );
    
} 
