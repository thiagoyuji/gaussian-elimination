#include "gaussian_elimination.h"
#include "../validation/validation.h"
#include "../../protocol/protocol.h"
#include "../linear_expressions_matrix/conversor/conversor.h"
#include "../algorithm/algorithm.h"

static struct GaussianEliminationResponse responseErrorInvalidRequest();

struct GaussianEliminationResponse applyGaussianEliminationOn( struct GaussianEliminationRequest request ){
    
    if( !validateRequest( request ) ) return responseErrorInvalidRequest();
    
    struct LinearExpressionsMatrix matrix = convert( request );
    
    struct GaussianEliminationResponse response = gaussianEliminationAlgorithm( matrix );
    
    freeMatrix( matrix );
    
    return response;
    
}

static struct GaussianEliminationResponse responseErrorInvalidRequest(){
    
    struct GaussianEliminationResponse response = {
        .code = INVALID_REQUEST,
        .message = INVALID_REQUEST_MESSAGE,
        .calculationResults = ""
    };
    
    return response;
    
}
