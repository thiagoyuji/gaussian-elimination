#ifndef GAUSSIAN_ELIMINATION_H_INCLUDED
#define GAUSSIAN_ELIMINATION_H_INCLUDED

#include "../input/gaussianEliminationRequest.h"
#include "../output/gaussianEliminationResponse.h"

struct GaussianEliminationResponse applyGaussianEliminationOn( struct GaussianEliminationRequest request );

#endif
