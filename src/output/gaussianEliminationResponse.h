#ifndef GAUSSIAN_ELIMINATION_RESPONSE_H_INCLUDED
#define GAUSSIAN_ELIMINATION_RESPONSE_H_INCLUDED

struct GaussianEliminationResponse {
  
    int code;
    
    char * message;
    
    char * calculationResults;
    
};

#endif
