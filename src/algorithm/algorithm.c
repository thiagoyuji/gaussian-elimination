#include <stdio.h>
#include <string.h>

#include "algorithm.h"
#include "../../protocol/protocol.h"
#include "../../include/boolean/boolean.h"
#include "../predicate/predicate.h"
#include "../../include/zero.h"

#define GHOST_VALUE -1
#define SPACE_PER_RESULT 1000

struct GaussianEliminationResponse successAnswer( char result[SPACE_PER_RESULT] );
struct GaussianEliminationResponse cannotScaleMatrixAnswer();

static boolean predicatePivotEqualZero( float pivot );
static boolean scaleMatrix( struct LinearExpressionsMatrix matrix );
static boolean predicateLineNumberEqualGhost( float lineNumber );

static int searchNewLineToPivot( struct LinearExpressionsMatrix matrix, int columnIndex );

static float scaleFunction( float constantK, float constantW, float indexILine, float indexJLine );
static float calculateIcognitoValue( struct LinearExpressionsMatrix matrix, int line );

static void swap( float * a, float * b );
static void swapLines( float * lineA, float * lineB, int numberOfColumns );
static void putZeroInColumns( struct LinearExpressionsMatrix matrix, int index );
static void calculateValuesResult( struct LinearExpressionsMatrix matrix, char * resultString );
static void ajustMatrixToCalc( struct LinearExpressionsMatrix matrix, int line, int column, float icognitoValue );

struct GaussianEliminationResponse gaussianEliminationAlgorithm( struct LinearExpressionsMatrix matrix ){
    
    boolean scaled = scaleMatrix( matrix );
    
    if( !scaled ) return cannotScaleMatrixAnswer();
    
    char result[ SPACE_PER_RESULT ];
    
    calculateValuesResult( matrix, result );
    
    struct GaussianEliminationResponse response = successAnswer( result );
    
    return response;
    
}

static boolean scaleMatrix( struct LinearExpressionsMatrix matrix ){
    
    int index = 0;
    
    boolean check = TRUE;
    
    while( check == TRUE && index < ( matrix.numberOfLines - 1 ) ){

        if( predicatePivotEqualZero( matrix.linearExpressions[ index ][ index ] ) ){
            
            int newLine = searchNewLineToPivot( matrix, index );
            
            if( predicateLineNumberEqualGhost( newLine ) ) check = FALSE;
            else swapLines( matrix.linearExpressions[ index ] , matrix.linearExpressions[ newLine ], matrix.numberOfColumns );
    
        }
        
        putZeroInColumns( matrix, index );
        
        index++;

    }
    
    return check;
    
}

static void calculateValuesResult( struct LinearExpressionsMatrix matrix, char * resultString ){
    
    int line = 0;
    
    for( line = (matrix.numberOfLines - 1); line >= 0; line-- ){
        
        char result[ SPACE_PER_RESULT ] = "";
        
        float icognitoValue = calculateIcognitoValue( matrix, line );
        
        snprintf(result, SPACE_PER_RESULT, "[x%d = %.3f]", line + 1, icognitoValue );
        
        strcat( resultString, result ); 
                
    }
    
}

struct GaussianEliminationResponse successAnswer( char result[SPACE_PER_RESULT] ){
    
    struct GaussianEliminationResponse response = {
        .code = SUCCESS,
        .message = SUCCESS_MESSAGE,
        .calculationResults = result
    };
    
    return response;
    
}

struct GaussianEliminationResponse cannotScaleMatrixAnswer(){
    
    struct GaussianEliminationResponse response = {
        .code = CANNOT_SCALE_MATRIX,
        .message = CANNOT_SCALE_MATRIX_MESSAGE,
        .calculationResults = ""
    };
    
    return response;
    
}

static void ajustMatrixToCalc( struct LinearExpressionsMatrix matrix, int line, int column, float icognitoValue ){
    
    int count = 0;
        
    for( count = 0; count < line; count++ ){ 
        matrix.linearExpressions[ count ][ column ] *= (icognitoValue * -1); 
    }
    
}

static float calculateIcognitoValue( struct LinearExpressionsMatrix matrix, int line ){
    
    float soma = 0;
    
    int column = matrix.numberOfColumns - 1;
    
    while( column >= 1 && column > line ){
        
        soma += matrix.linearExpressions[ line ][ column ];
        
        column--;
        
    }
    
    float icognitoValue = soma / matrix.linearExpressions[ line ][ line ];
    
    ajustMatrixToCalc( matrix, line, column, icognitoValue );
    
    return icognitoValue;
    
}

static boolean predicatePivotEqualZero( float pivot ){
	return pivot == ZERO;
}

static boolean predicateLineNumberEqualGhost( float lineNumber ){
    return lineNumber == GHOST_VALUE;    
}

static int searchNewLineToPivot( struct LinearExpressionsMatrix matrix, int columnIndex ){

    int count = 0;

    boolean check = TRUE;

    while( check == TRUE && count < matrix.numberOfLines ){

        if( !predicateIntegerEqualZero( matrix.linearExpressions[count][columnIndex] ) ) check = FALSE;
        
        count++;

    }
    
    if( check ) return GHOST_VALUE;
    
    return (count - 1);

}

static void swap( float * a, float * b ){

    int aux = *a;
    
    *a = *b;
    
    *b = aux;

}

static void swapLines( float * lineA, float * lineB, int numberOfColumns ){

    int count = 0;
    
    for( count = 0; count < numberOfColumns; count++ ){
    
        swap( &lineA[count], &lineB[count] );
    
    }

}

static float scaleFunction( float constantK, float constantW, float indexILine, float indexJLine ){
    return (constantK * indexILine) - (constantW * indexJLine); 
}

static void putZeroInColumns( struct LinearExpressionsMatrix matrix, int index ){

    int line = 0;
    int column = 0;

    for( line = (index + 1); line < matrix.numberOfLines; line++ ){

        if( !predicateIntegerEqualZero( matrix.linearExpressions[ line ][ index ] ) ){
    
            float constantK = matrix.linearExpressions[ index ][ index ];
            float constantW = matrix.linearExpressions[ line ][ index ];
            
            for( column = index; column < matrix.numberOfColumns; column++ ){
                
                float indexILine = matrix.linearExpressions[ line ][ column ];
                float indexJLine = matrix.linearExpressions[ index ][ column ];
                
                matrix.linearExpressions[ line ][ column ] = scaleFunction( constantK, constantW, indexILine, indexJLine );
                
            }
        
        }
    
    }

}
