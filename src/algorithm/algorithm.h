#ifndef GAUSSIAN_ELIMINATION_ALGORITHM_H_INCLUDED
#define GAUSSIAN_ELIMINATION_ALGORITHM_H_INCLUDED

#include "../output/gaussianEliminationResponse.h"
#include "../linear_expressions_matrix/matrix.h"

struct GaussianEliminationResponse gaussianEliminationAlgorithm( struct LinearExpressionsMatrix matrix );

#endif
