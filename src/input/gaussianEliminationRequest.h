#ifndef GAUSSIAN_ELIMINATION_INPUT_H_INCLUDED
#define GAUSSIAN_ELIMINATION_INPUT_H_INCLUDED

struct GaussianEliminationRequest {
  
    int numberOfLinearExpressions;

    int numberOfIcognitos;
    
    char * linearExpressions;
    
};

#endif
