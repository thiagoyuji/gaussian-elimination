#include <regex.h>
#include <stdio.h>
#include <stdlib.h>

#include "predicate.h"
#include "../../include/zero.h"

#define LESS_LIMIT 2

static boolean predicateValueBiggerOrEqualTo( int value, int comparisonValue );

static boolean predicateEqualIntegerValues( int valueOne, int valueTwo );

static boolean predicateStringWithInformation( char * string );

static boolean predicateStringMatchesARegularExpression( char * string, char * regularExpressionLinearExpression );

boolean predicateNumberOfLinearExpressionsValid( struct GaussianEliminationRequest request ){
    
    if( !predicateValueBiggerOrEqualTo( request.numberOfLinearExpressions, LESS_LIMIT ) ) return FALSE;
    
    return TRUE;
    
}

boolean predicateNumberOfIcognitosValid( struct GaussianEliminationRequest request ){
    
    if( !predicateValueBiggerOrEqualTo( request.numberOfIcognitos, LESS_LIMIT ) ) return FALSE;
    
    if( !predicateEqualIntegerValues( request.numberOfLinearExpressions, request.numberOfIcognitos ) ) return FALSE;
    
    return TRUE;
    
}

boolean predicateStringLinearExpressionsValid( struct GaussianEliminationRequest request ){
    
    char regularExpression[70];
    
    sprintf(regularExpression, "^(([\\+--0-9]+ ){%d}[\\+--0-9]+\\/){%d}(([\\+--0-9]+ ){%d}[\\+--0-9]+)$", 
            request.numberOfIcognitos, request.numberOfLinearExpressions - 1, request.numberOfIcognitos);

    if( request.linearExpressions == NULL ) return FALSE;

    if( !predicateStringWithInformation( request.linearExpressions ) ) return FALSE;

    if( !predicateStringMatchesARegularExpression( request.linearExpressions , regularExpression) ) return FALSE;

    return TRUE;
    
}

static boolean predicateValueBiggerOrEqualTo( int value, int comparisonValue ){
    
    if( value >= comparisonValue ) return TRUE;
    
    return FALSE;
    
}

static boolean predicateEqualIntegerValues( int valueOne, int valueTwo ){
 
    if( valueOne == valueTwo ) return TRUE;
    
    return FALSE;
    
}

boolean predicateIntegerEqualZero( int value ){
    
    if( value == ZERO ) return TRUE;
    
    return FALSE;
    
}

static boolean predicateStringWithInformation( char * string ){
    
    if( string[0] == '\0' ) return FALSE;
    
    return TRUE;
    
}

static boolean predicateStringMatchesARegularExpression( char * string, char * regularExpressionLinearExpression ){
     
    regex_t regularExpression;
    
    int resultRegexCompilation = regcomp( &regularExpression , regularExpressionLinearExpression, REG_EXTENDED );
    
    if( !predicateIntegerEqualZero( resultRegexCompilation ) ) return FALSE;

    int resultRegexExecution = regexec( &regularExpression, string, 0, (regmatch_t *)NULL, 0);

    if( !predicateIntegerEqualZero( resultRegexExecution ) ) return FALSE;
    
    regfree( &regularExpression );
    
    return TRUE;
    
}
