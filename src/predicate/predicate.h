#ifndef PREDICATE_H_INCLUDED
#define PREDICATE_H_INCLUDED

#include "../../include/boolean/boolean.h"
#include "../input/gaussianEliminationRequest.h"

boolean predicateNumberOfLinearExpressionsValid( struct GaussianEliminationRequest request );

boolean predicateNumberOfIcognitosValid( struct GaussianEliminationRequest request );

boolean predicateStringLinearExpressionsValid( struct GaussianEliminationRequest request );

boolean predicateIntegerEqualZero( int value );

#endif
