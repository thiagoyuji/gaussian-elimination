#ifndef VALIDATION_RESPONSE_H_INCLUDED
#define VALIDATION_RESPONSE_H_INCLUDED

#include "../../include/boolean/boolean.h"
#include "../input/gaussianEliminationRequest.h"

boolean validateRequest( struct GaussianEliminationRequest request );

#endif
