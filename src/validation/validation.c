#include "validation.h"
#include "../predicate/predicate.h"

boolean validateRequest( struct GaussianEliminationRequest request ){
    
    if( !predicateNumberOfLinearExpressionsValid( request ) ) return FALSE;

    if( !predicateNumberOfIcognitosValid( request ) ) return FALSE;

    if( !predicateStringLinearExpressionsValid( request ) ) return FALSE;

    return TRUE;
    
}

