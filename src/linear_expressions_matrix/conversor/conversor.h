#ifndef CONVERSOR_STRING_LINEAR_EXPRESSIONS_MATRIX_H_INCLUDED
#define CONVERSOR_STRING_LINEAR_EXPRESSIONS_MATRIX_H_INCLUDED

#include "../matrix.h"
#include "../../input/gaussianEliminationRequest.h"

struct LinearExpressionsMatrix convert( struct GaussianEliminationRequest request );

#endif
