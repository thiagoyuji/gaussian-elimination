#include "conversor.h"
#include "../../../include/string/string.h"

#include <stdio.h>

#define NUMBER_OF_RESULTS 1

#define TOKEN_BAR '/'
#define TOKEN_SPACE ' '

void mapLinearExpressionStringToMatrix( struct LinearExpressionsMatrix matrix, char * linearExpressions[] );

struct LinearExpressionsMatrix convert( struct GaussianEliminationRequest request ){
    
    struct LinearExpressionsMatrix matrix = createMatrix( request.numberOfLinearExpressions, request.numberOfIcognitos + NUMBER_OF_RESULTS );
    
    char * linearExpressions[ request.numberOfLinearExpressions ];
    
    alocateMemoryToStringPointerList( linearExpressions, request.numberOfLinearExpressions, strlen(request.linearExpressions) );
    
    splitStringByToken( linearExpressions, request.linearExpressions, TOKEN_BAR );
    
    mapLinearExpressionStringToMatrix( matrix, linearExpressions );
    
    freeMemoryFromStringPointerList( linearExpressions, request.numberOfLinearExpressions );
    
    return matrix;
    
}

void mapLinearExpressionStringToMatrix( struct LinearExpressionsMatrix matrix, char * linearExpressions[] ){
    
    int numbers[ matrix.numberOfLines ];
    
    int line = 0;
    
    int column = 0;
    
    for( line = 0; line <  matrix.numberOfLines; line++ ){
        
        mapStringNumbersInNumbersListByToken( numbers, linearExpressions[ line ], TOKEN_SPACE );
        
        for( column = 0; column < matrix.numberOfColumns; column++ ){
            
            matrix.linearExpressions[ line ][ column ] = numbers[ column ];
            
        }
        
    }
    
}
