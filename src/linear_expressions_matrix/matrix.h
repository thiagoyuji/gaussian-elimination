#ifndef MATRIX_H_INCLUDED
#define MATRIX_H_INCLUDED

struct LinearExpressionsMatrix{
    
    int numberOfLines;
    
    int numberOfColumns;
    
    float ** linearExpressions;
    
};

struct LinearExpressionsMatrix createMatrix( int lines, int columns );

void freeMatrix( struct LinearExpressionsMatrix matrix );

#endif
