#include <stdlib.h>

#include "matrix.h"

struct LinearExpressionsMatrix createMatrix( int lines, int columns ){
    
    struct LinearExpressionsMatrix matrix = {
        .numberOfLines = lines,
        .numberOfColumns = columns,
        .linearExpressions = ( float ** ) malloc( lines * sizeof( float * ) )
    };
    
    int count = 0;
    
    for( count = 0; count < lines; count++ ){
        
        matrix.linearExpressions[ count ] = ( float * ) calloc( columns, sizeof( float ) );
        
    }
    
    return matrix;
    
}

void freeMatrix( struct LinearExpressionsMatrix matrix ){
 
    int count = 0;
    
    for( count = 0; count < matrix.numberOfLines; count++ ){
        
        free( matrix.linearExpressions[ count ] );
        
    }
    
    free( matrix.linearExpressions );
    
}
