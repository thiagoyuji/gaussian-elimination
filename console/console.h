#ifndef CONSOLE_H_INCLUDED
#define CONSOLE_H_INCLUDED

#include "../src/output/gaussianEliminationResponse.h"
#include "../src/input/gaussianEliminationRequest.h"

struct GaussianEliminationRequest input();

void output( struct GaussianEliminationResponse response );

#endif
