#include <stdio.h>

#include "console.h"

struct GaussianEliminationRequest input(){
    
    struct GaussianEliminationRequest request = {
        .numberOfLinearExpressions = 0,
        .numberOfIcognitos = 0,
        .linearExpressions = ""
    };
    
    return request;
    
}

void output( struct GaussianEliminationResponse response ){
    
    printf("Code : %d\n", response.code);
    printf("Message : %s\n", response.message);
    printf("Result : %s\n", response.calculationResults);
    
}
